package com.example.tp2_ressources_timer;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    int var = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String ch = getString( R.string.legit);
        Log.v("Log",""+ch);

        Resources res = getResources();
        Timer chrono = new Timer();

        Button bouton_change = (Button) findViewById( R.id.button);
        ImageView change_image = (ImageView) findViewById( R.id.imageView);

        bouton_change.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            public void onClick(View v) {
                if (var == 0) {
                    change_image.setImageResource(R.drawable.skyrim);
                    //bouton_change.getBackground().setTint( R.color.vert);

                    var++;
                }
                else if (var == 1) {
                    change_image.setImageResource(R.drawable.horde);

                    var--;
                }
            }

        });


        chrono.schedule(new TimerTask(){
            boolean a = false;
            public void run(){
                if (a==false) {
                    bouton_change.setBackgroundColor(getResources().getColor(R.color.vert));
                    a = true;}
                else if (a==true){
                    bouton_change.getBackground().setTint(Color.parseColor( "#FFE800"));
                    a = false;}
            }
        },5000, 5000);
    }
}