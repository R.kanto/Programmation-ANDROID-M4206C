package com.example.tp4activityintent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DemandeNom extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demande_nom);

        Button ok_nom = (Button) findViewById(R.id.button4);
        TextView ecrire = (TextView) findViewById(R.id.editTextTextPersonName);
        ok_nom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("Log", ecrire.getText().toString());
            }
        });
    }
}