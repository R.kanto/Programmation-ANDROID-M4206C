package com.example.tp3;

import android.os.Message;
import android.widget.ImageView;
import android.os.Handler;



public class MonHandler extends Handler {


    Message msg = Message.obtain();
    ImageView targetimage;

    public void handleMessage(Message msg) {
        super.handleMessage(msg); //on remonte l’appel vers la classe mère
        Integer val = (Integer) msg.obj; //on récupère la donnée fixée
        //sur l’attribut obj
        //... on exploite de cette donnée...
    }

    public void setTargetimage(ImageView id) {
        this.targetimage = id;

    }

}
