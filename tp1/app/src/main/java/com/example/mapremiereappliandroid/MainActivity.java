package com.example.mapremiereappliandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.*;
import android.view.*;

import static com.example.mapremiereappliandroid.R.id.textView;

public class MainActivity extends AppCompatActivity {
    int i=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //TODO: Faire le TP1
        final Button buttonPlus = (Button) findViewById(R.id.button);
        final Button buttonMoins = (Button) findViewById(R.id.button2);
        final Button buttonReset = (Button) findViewById(R.id.button3);
        TextView textView = (TextView) findViewById(R.id.textView);
        buttonPlus.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                i++;
                Log.v("LOG", "Clic : "+i);
                textView.setText("Nombre de clic : "+i);
            }

        });

        buttonMoins.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                i--;
                textView.setText("Nombre de clic : "+i);
            }

        });
        buttonReset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                i=0;
                textView.setText("Reset du nombre de clic\nNombre de clic : "+i);
            }

        });
        Log.v("LOG", "HelloWorld");

    }
}